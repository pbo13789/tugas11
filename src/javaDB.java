import java.sql.*;
import java.util.Scanner;

public class javaDB {
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/penjualan";
	static final String USER = "root";
	static final String PASS = "";

	static Connection conn;
	static Statement stmt;
	static ResultSet rs;
	static Scanner inp = new Scanner(System.in);

	public static void main(String[] args) {
		insert();
		show();
	}

	public static void insert() {
		System.out.print("Masukan Kode Barang : ");
		String kode_brg = inp.nextLine();
		System.out.print("Masukan Nama Barang : ");
		String nama_brg = inp.nextLine();
		System.out.print("Masukan Satuan Barang : ");
		String satuan = inp.nextLine();
		System.out.print("Masukan Stok Barang : ");
		int stok = inp.nextInt();
		System.out.print("Masukan Stok Minimal Barang : ");
		int stok_min = inp.nextInt();
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Conection oke");
			stmt = conn.createStatement();

			String sql = "INSERT INTO barang (kode_brg,nm_brg,satuan,stok_brg,stok_min) VALUES (?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, kode_brg);
			ps.setString(2, nama_brg);
			ps.setString(3, satuan);
			ps.setInt(4, stok);
			ps.setInt(5, stok_min);

			ps.execute();

			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void show() {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();

			rs = stmt.executeQuery("SELECT * FROM barang");
			int i = 1;
			while (rs.next()) {
				System.out.println("Data ke-" + i);
				System.out.println("Kode Barang: " + rs.getString("kode_brg"));
				System.out.println("Nama Barang: " + rs.getString("nm_brg"));
				System.out.println("Satuan: " + rs.getString("satuan"));
				System.out.println("Stok: " + rs.getString("stok_brg"));
				System.out.println("Stok minimal: " + rs.getString("stok_min"));
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
